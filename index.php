<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
            include_once('bootstrap.php');
        ?>
        <style>

            #formDelete{
                font-size: 20px;
                min-height: 60px;
                width: 100%;
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
                grid-template-rows: 1fr 1fr 1fr 1fr;
                grid-gap: 15px;
                max-width: 100%;
                max-height: 300px;
            }

            .product
            {
                border: 3px solid #555;
                padding: 10px;
                
            }
            .product-name
            {
                text-align: center;
                font-weight: bolder;
                font-size: 22px;
                word-wrap: break-word
            }
            .product-info
            {
                text-align: center;
                font-weight: lighter;
                font-size: 19px;
                word-wrap: break-word
            }
            .topnav
            {
                display: flex;
                border-bottom: 2px solid #444;
                margin-bottom: 4px;
                text-align: right;
                justify-content: space-between;
            }
            #buttons
            {
                text-align: center;
                position:static;
                flex-direction: right;
            }

            #delete-product-btn{
                display: inline;
                background: crimson;
                
            }

            .btn
            {
                color: #FFF;
                padding: 8px 12px;
                text-decoration: none;
                text-align: center;
                align-content: center;
                vertical-align: middle;
            }
            #save
            {
                display: inline;
                margin-top: 3px;
                width: 122px;
                background: rgb(31, 50, 196);
            }
        
        #formInsert
        {
            padding-top: 1%;
            padding-right: 5%;
            display: inline-block;
        }

        </style>


    </head>



<body>

    <div class="topnav">
        <h1> Product List </h1>
        <div id="buttons">
            <form action="add-product.html" id="formInsert">
                <input type="submit" id="delete-product-btn" form="formDelete" value="MASS DELETE" class="btn"  >
                <input type="submit" value="ADD" class="btn" id="save">
            </form>
        </div>
        
        
        <iframe name="dummyframe" id="dummyframe" style="display: none;"></iframe>
    </div>
    
    <?php
        echo \Scandi\DataBase\HtmlRetriever::getProductList();
    ?>

</body>
