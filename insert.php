<?php

    include_once('bootstrap.php');
    use Scandi\DataBase\Fetcher;
    $fetcher = new Fetcher(); //PSR-12 states that one is allowed to create blank lines if serves visibility
    
    if(isset($_POST['sku']))
    {
        $product = \Scandi\Products\ProductFactory::makeProduct(
            $_POST['sku'],
            $_POST['name'],
            $_POST['price'],
            $_POST['weight'],
            $_POST['size'],
            $_POST['length'],
            $_POST['height'],
            $_POST['width']
        );
        $fetcher->insertProduct($product);         
    }