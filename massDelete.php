<?php
    include_once('bootstrap.php'); //does this count as a violation of psr-1??? i scream, for i don't know.

    use Scandi\DataBase\Fetcher;

    $fetcher = new Fetcher();

    if(isset($_POST['products']))
    {
        $products = $_POST['products'];
        foreach($products as $product)
        {
            $fetcher->deleteProduct($product); //i could make the an instance of the product using hidden fields, but this is more efficient and pretty safe for this application i believe.
        }
    }