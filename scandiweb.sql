
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `scandiweb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `scandiweb`;

CREATE TABLE `products` (
  `sku` varchar(20) NOT NULL,
  `name` varchar(40) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `size` int(11) DEFAULT NULL,
  `weight` decimal(9,3) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `products`
  ADD PRIMARY KEY (`sku`);
COMMIT;