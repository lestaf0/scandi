<?php 
   
    namespace Scandi\DataBase;
    use mysqli_result;
    class Crud extends DbConnect 
    { 
        protected $connection;

        public function __construct()
        {
            $this->connection = parent::getConnection();
        }
                 
        public function selectalldata($table = 'products'):array
        { 
        	$select="SELECT * FROM $table"; 
            $select1 = $this->connection->query($select); 
            $array = $select1->fetch_all(MYSQLI_ASSOC);
            //$select1->free_result();
            return $array; 
        } 
                        
        public function selectbysku($table,$sku) 
        { 
            $sel= "SELECT * FROM $table where sku=$sku"; 
            $sel1=$this->connection->query($this->escape_string($sel)); 
            return $sel1;                              
        } 
                        
        public function insert($data,$table = 'products') 
        { 
            /*$this->columns = "";
            $this->values = "";
                                
            foreach($data as $this->column => $this->value) 
            {                                 
                $this->columns .= ($this->columns == "") ? "" : ", "; 
                $this->columns .= $this->column; 
                $this->values .= ($this->values == "") ? "" : ", ";
                if(gettype($this->value) == 'string'){
                    $this->values .= "'".$this->escape_string($this->value)."'"; 
                }else{
                    $this->values .= $this->value; 
                }
            }*/                      
            $insert= ("INSERT into $table (".implode(",",array_keys($data->getProperties())).") VALUES ('".implode("', '",$data->getProperties())."')");
            echo $insert;
            $insert1= $this->connection->query($insert);                                  
        } 
                                                
        public function deleteBySku($sku, $table = 'products') 
        {
            $delete=("DELETE FROM $table WHERE sku=\"$sku\""); 
            $this->connection->query($delete); 
            return true; 
        } 
                        
        public function escape_string($value) 
        { 
            return $this->connection->real_escape_string($value); 
		}          
    }
   