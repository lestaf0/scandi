<?php

    namespace Scandi\DataBase;
    class Dbconnect 
    { 
        private $localhost = 'localhost'; 
        private $user = 'root'; 
        private $password = ''; 
        private $dbname = 'scandiweb'; 
        protected $connection; 

        public function getConnection() 
        { 
            if(!isset($this-> connection)) 
            {       
                $this->connection = new \mysqli(
                $this->localhost, 
                $this->user,
                $this->password,
                $this->dbname
            );
            } 
          return $this->connection; 
          } 
    } 
