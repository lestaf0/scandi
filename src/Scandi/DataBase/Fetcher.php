<?php

    namespace Scandi\DataBase;
    
    class Fetcher
    {
        public function selectAll()
        {
            $crud = new Crud();
            $array = $crud->selectalldata("products");
            $arrayret = [];
            foreach($array as $product)
            {
                $arrayret[] = \Scandi\Products\ProductFactory::makeProduct(
                    $product['sku'],
                    $product['name'],
                    $product['price'],
                    $product['weight'],
                    $product['size'],
                    $product['length'],
                    $product['height'],
                    $product['width'],
                );
            }   
            return $arrayret;
        }
        public function insertProduct($product)
        {
            //$data = $product->getProperties();
            $crud = new Crud();
            $crud->insert($product);
        }
        public function deleteProduct($sku)
        {
            $crud = new Crud();
            $crud->deleteBySku($sku);
        }
    }