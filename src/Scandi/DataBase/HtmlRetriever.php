<?php

    use Scandi\DataBase\Fetcher;
    namespace Scandi\DataBase;
    use Scandi\Products\Product;
    class HtmlRetriever
    {
        public static function getProductList():string
        {
            $fetcher = new Fetcher();
            $htmlString = "<form action=\"massDelete.php\" id=\"formDelete\" method=\"POST\" target=\"dummyframe\" onsubmit=\"setTimeout(function() { window.location.reload(); }, 5)\">"; //cant i do this on the index already?
            $arr = $fetcher->selectAll();
            foreach($arr as $product)
            {
                if(!is_null($product))
                {
                    $htmlString = $htmlString.''.$product->getHTML();
                }
            }
            $htmlString .= "</form>";
            return $htmlString;
        }
    }
