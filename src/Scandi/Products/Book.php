<?php

    namespace Scandi\Products;
    class Book extends Product
    {
        private float $weight;
        public function __construct(
            string $sku,
            string $name,
            float $price,
            float $weight
        ){ 
            parent::__construct($sku,$name,$price);
            $this->weight = $weight;
        }
        public function setWeight($weight)
        {
            $this->weight = $weight;
        }
        public function getWeight()
        {
            return $this->weight;   
        }
        public function getProperties(): array
        {
            return array_merge(parent::getProperties(), get_object_vars($this));  
        }
        public function getHTML()
        {
            return parent::getHTML()."Weight: $this->weight g
            </div>
            </div>
            ";
        }
    }