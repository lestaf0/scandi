<?php

    namespace Scandi\Products;
    class Dvd extends Product
    {
        private int $size;
        public function __construct(string $sku, string $name, float $price, int $size)
        {
            parent::__construct($sku, $name, $price);
            $this->size = $size;    
        }
        public function getSize()
        {
            return $this->size;
        }
        public function setSize($size)
        {
            $this->size = $size;
        }
        public function getProperties(): array
        {
            return array_merge(parent::getProperties(), get_object_vars($this)); 
        }
        public function getHTML()
        {
            return parent::getHTML()."Size: $this->size MBs
            </div>
            </div>";
        }
    }
