<?php

    namespace Scandi\Products;
    class Furniture extends Product
    {
        private $height;
        private $width;
        private $length;
        public function __construct(
            string $sku,
            string $name,
            float $price,
            int $height,
            int $width,
            int $length
        ){
            parent::__construct($sku,$name,$price);
            $this->height = $height;
            $this->width = $width;
            $this->length = $length;
        }
        public function getHeight()
        {
            return $this->height;
        }
        public function getWidth()
        {
            return $this->width;   
        }
        public function getLength()
        {
            return $this->length;   
        }
        public function setHeight($height)
        {
            $this->height = $height;
        }
        public function setWidth($width)
        {
            $this->width = $width;
        }
        public function setLength($length)
        {
            $this->length = $length;
        }
        public function getProperties(): array
        {
            return array_merge(parent::getProperties(), get_object_vars($this)); 
        }
        public function getHTML()
        {
            return parent::getHTML()."Length: $this->length cm<br>
            Width: $this->width cm<br>
            Height: $this->height cm
            </div>
            </div>";
        }
    }
