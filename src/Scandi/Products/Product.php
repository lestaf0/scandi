<?php

    namespace Scandi\Products;
    abstract class Product
    {
        private $sku;
        private $name;
        private float $price;

        public function __construct(string $sku,string $name,float $price)
        {   
            $this->sku = $sku;
            $this->name = $name;
            $this->price = $price;       
        }
        public function getSku()
        {
            return $this->sku; 
        }
        public function getName()
        {
            return $this->name;
        }
        public function getPrice()
        {
            return $this->price;
        }
        public function setSku($sku)
        {
            $this->sku = $sku;      
        }
        public function setName($name)
        {
            $this->name = $name;      
        }
        public function setPrice($price)
        {
            $this->price = $price;       
        }   
        public function getHTML()
        {
            return "<div class=\"product\" id=\"$this->sku\">  
                <input type=\"checkbox\" name=\"products[]\" class=\"delete-checkbox\" value=\"$this->sku\">
                <div class=\"product-name\">$this->name</div>
                <div class=\"product-info\">Price: $$this->price<br>";
                //make the checkbox in here already, with the value being the sku. OR have another class do it when composing the HTML
        }  
        public function getProperties(): array
        {
            return get_object_vars($this);
        }
    }
?>
