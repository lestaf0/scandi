<?php

    namespace Scandi\Products;
    class ProductFactory
    {
        public static function makeProduct(
            $sku,
            $name,
            $price,
            $weight,
            $size,
            $length,
            $height,
            $width
        ){
            $sku = (string) $sku;
            $name = (string) $name;
            $price = (float) $price;
            if(!empty($weight) && !is_null($weight))
            {
                $weight = (float) $weight;
                return new Book($sku,$name,$price,$weight);
            }
            if(!empty($size) && !is_null($size))
            {
                $size = (int) $size;
                return new Dvd($sku,$name,$price,$size);
            }
            if(!empty($length) && !is_null($length) && !empty($height) 
                && !is_null($height) && !empty($height) && !is_null($height))
            {
                $height = (int) $height;
                $length = (int) $length;
                $width  = (int) $width;
                return new Furniture($sku,$name,$price,$height,$width, $length);
            }else
            {
                return null;
            }
        }
    }
